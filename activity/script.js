"use strict";

let trainer = {
    name : "Jonathan Cruz",
    Age : 25,
    friends : {
        kanto : ["Brock","Misty"],
        Kalos : ["Serena", "Clemont"]
    },
    pokemon : ["Pikachu","Arcanine","Nidoking","Rattata"],
    talk : function(){
        console.log("Pikachu! I choose you!")
    }
}

console.log(trainer)
console.log("Result of Dot Notation:")
console.log(trainer.name)
console.log("Result of Square Bracket Notation:")
console.log(trainer["pokemon"])
console.log("Result of Talk Method")
trainer.talk()

function pokemon(name,level,health,attack){
    this.name = name
    this.level = level
    this.health = health
    this.attack = attack
    this.tackle = function(){
        console.log(`${this.name} tackled ${enemyPokemon.name}`)
        this.faint()
        enemyPokemon.health = enemyPokemon.health - this.attack
        console.log(`${enemyPokemon.name} is now ${enemyPokemon.health}`)
    }
    this.faint = function(){
        if(enemyPokemon.health <= 0){
            console.log(`^^^^${enemyPokemon.name} has fainted^^^^`)
        }
    }
}


console.log("---------My Pokemon---------")
let pokemonPikachu = new pokemon("Pikachu", 25, 50, 20)
let pokemonArcanine = new pokemon("Arcanine", 50, 100, 40)
let pokemonRattata = new pokemon("Rattata", 100, 200, 100)

console.log(pokemonPikachu)
console.log(pokemonArcanine)
console.log(pokemonRattata)

console.log("---------Enemy Pokemon---------")
let enemyPokemon = new pokemon("Shadow Lugia",100,200,100)
console.log(enemyPokemon)

console.log("---------Battling Area---------")
pokemonPikachu.tackle()
pokemonRattata.tackle()
pokemonArcanine.tackle()
pokemonArcanine.tackle()
pokemonArcanine.tackle()


